from random import randint

print("Bienvenido a La Maquina Expendedora 3000")
linea = " "
data = " "
Select = 0
dinero = float(input("Por favor introduzca su dinero:")) #Este sera el dinero utilizado para comprar los aperitivos
"""
    Donde se Introduce el dinero
"""
#Funciones


def compra (dinero_necesario):  # Para la facilidad personal. Simplemente realiza un seguimiento del dinero despues de verificar para ver si puede comprar dicho articulo.

    """
    Funcion que verifica si el usuario tiene suficiente dinero, y modifica el inventario si se puede hacer la compra
    :param dinero_necesario:
    :return:
    """

    global dinero
    global Select
    global linea
    global data

    if dinero >= dinero_necesario:
        if int(data[3]) > 0:
            dinero -= dinero_necesario
            print("Articulo Vendido.")
            data[3] = str(int(data[3]) - 1)
            with open("Inventario.txt", "w") as fw:
                linea[Select] = str(data[0] + "," + data[1] + "," + data[2] + "," + data[3] + "\n")
                fw.writelines(linea)
        else:
            print("No hay mas del producto deseado")
    else:
        print("Dinero insuficiente.")


def transaccion(entrada_usuario): #La funcion esta hecha para elegir dicho elemento, luego llama a la funcion de "compra"

    """
     Funcion que se sencarga de confirmar que eleccion hizo el usuario
    :param entrada_usuario:
    :return:
    """

    global dinero
    global Select
    global linea
    global data


    with open("Inventario.txt", "r") as fr:
        linea = fr.readlines()
        if entrada_usuario == "0A":
            Select = 0
            data = linea[0].split(",")
            compra(float(data[2]))

        elif entrada_usuario == "0B":
            Select = 1
            data = linea[1].split(",")
            compra(float(data[2]))

        elif entrada_usuario == "0C":
            Select = 2
            data = linea[2].split(",")
            compra(float(data[2]))

        elif entrada_usuario == "0D":
            Select = 3
            data = linea[3].split(",")
            compra(float(data[2]))

        else:
            print("Entrada Invalida.")


#==============================================================


def main(): #Main

    """
    Funcion principal del programa, imprime la lista de elecciones, sus codigos y sus precios
    :return:
    """

    global dinero
    cambio = 1
    while cambio == 1:
#Mensaje de Bienvenida

        print("Aqui estan sus opciones y los precios")
        while True:
            f = open("Inventario.txt", "r")
            linea = f.readline()
            while linea:
                data = linea.split(",")
                print(data[0] + " " + data[1] + " $" + data[2])
                linea = f.readline()
            f.close()
            break

        cambio_usuario= 1 #Bucle de prueba de usuario
        while cambio_usuario == 1:
            print("Actualmente tiene $" + str(dinero))
            entrada_usuario = input("Por favor ingrese el codigo de su seleccion: ").upper()
            transaccion(entrada_usuario)
            print()
            eleccion = 1
            while eleccion == 1: #Bucle de prueba de usuario
                entrada_usuario = input("Ha terminado con la maquina?(y/n): ").lower()

                if entrada_usuario == "y":

                    chance = randint(0,9)

                    if chance > 4:
                        print("Aqui esta su vuelto: " + str(dinero))

                    else:
                        print("Me comi su vuelto de " + str(dinero) + " . Estaba rico.")

                    cambio_usuario = 0
                    eleccion = 0
                    cambio = 0

                elif entrada_usuario == "n":
                    cambio_usuario = 1
                    eleccion = 0
                else:
                    print("Comando Invalido")
                    eleccion = 1


main()
