.. maquina documentation master file, created by
   sphinx-quickstart on Wed Aug 22 17:43:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MAQUINA EXPENDEDORA PROYECTO:
===================================

El proyecto contiene las los Funciones:

- Compra
- Transaccion



.. toctree::
   :maxdepth: 2

.. automodule:: Maquina
    :members:
    :special-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
